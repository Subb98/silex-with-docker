### Description
The base of your future Silex application with PostgreSQL and Docker on board

### Included
- PHP 7.1
- PostgreSQL
- Composer
- Silex

### Requirements
- Docker
- Git

### Usage
1. Run this commands:
    ```
git clone git@bitbucket.org:Subb98/silex-with-docker.git
cd silex-with-docker
docker-compose up
docker-compose exec web /usr/local/bin/composer install
```
2. Go to [http://localhost:3000](http://localhost:3000) in your browser
3. Enjoy!

<?php

namespace App;

class Subject
{
    private $name;
    private $observers = [];

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function attach(Observer $observer): void
    {
        $this->observers[] = $observer;
    }

    public function doSomething(): void
    {
        /** Do something...
         *
         * Notify observers that we did something */
        $this->notify('something');
    }

    public function doSomethingBad(): void
    {
        foreach ($this->observers as $observer) {
            $observer->reportError(42, 'Something bad happened', $this);
        }
    }

    protected function notify($argument): void
    {
        foreach ($this->observers as $observer) {
            $observer->update($argument);
        }
    }
}

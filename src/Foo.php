<?php

namespace App;

class Foo
{
    private $bar = [];

    public function addBar(string $name): void
    {
        $this->bar[] = new Bar($name);
    }

    public function getBar(): array
    {
        return $this->bar;
    }
}

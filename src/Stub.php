<?php

namespace App;

class Stub
{
    public function reallyLongTime(): array
    {
        $result = [1, 'foo', 'bar'];
        sleep(100);

        return $result;
    }
}

<?php

namespace App;

class Observer
{
    public function update(string $argument): void
    {
        // Do something
    }

    public function reportError(int $errorCode, string $errorMessage, Subject $subject): void
    {
        // Do something
    }
}

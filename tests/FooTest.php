<?php
declare(strict_types=1);

namespace Test;

use App\Foo;
use App\Bar;
use PHPUnit\Framework\TestCase;

final class FooTest extends TestCase
{
    public function testGetBar(): void
    {
        $foo = new Foo();
        $foo->addBar('Yoba');

        $bar = $foo->getBar();

        foreach ($bar as $obj) {
            $this->assertInstanceOf(Bar::class, $obj);
            $this->assertEquals($obj->getName(), 'Yoba');
        }
    }
}

<?php
declare(strict_types=1);

namespace Test;

use App\Observer;
use App\Subject;
use PHPUnit\Framework\TestCase;

class SubjectTest extends TestCase
{
    public function testObserversAreUpdated(): void
    {
        $observer = $this->getMockBuilder(Observer::class)
                         ->setMethods(['update'])
                         ->getMock();

        $observer->expects($this->once())
                 ->method('update')
                 ->with($this->equalTo('something'));

        $subject = new Subject('My subject');
        $subject->attach($observer);
        $subject->doSomething();
    }
}

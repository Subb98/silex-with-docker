<?php
declare(strict_types=1);

namespace Test;

use App\Stub;
use PHPUnit\Framework\TestCase;

final class StubTest extends TestCase
{
    public function testReallyLongTime(): void
    {
        $return = [1, 'foo', 'bar'];

        $stub = $this->createMock(Stub::class);
        $stub->method('reallyLongTime')
             ->willReturn($return);

        $result = $stub->reallyLongTime();

        $this->assertGreaterThan(0, count($result));
        $this->assertEquals($result, $return);
    }
}

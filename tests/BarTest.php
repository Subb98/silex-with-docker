<?php
declare(strict_types=1);

namespace Test;

use App\Bar;
use PHPUnit\Framework\TestCase;

final class BarTest extends TestCase
{
    public function testBarCreate(): void
    {
        $bar = new Bar('FizzBuzz');
        $this->assertInstanceOf(Bar::class, $bar);
        $this->assertEquals($bar->getName(), 'FizzBuzz');
    }
}
